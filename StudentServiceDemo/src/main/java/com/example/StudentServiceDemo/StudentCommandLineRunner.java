package com.example.StudentServiceDemo;

import com.example.StudentServiceDemo.model.Student;
import com.example.StudentServiceDemo.model.Subject;
import com.example.StudentServiceDemo.repository.StudentRepository;
import com.example.StudentServiceDemo.repository.SubjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.lang.reflect.Array;
import java.util.List;

@Component
public class StudentCommandLineRunner implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(StudentCommandLineRunner.class);

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    SubjectRepository subjectRepository;

    @Override
    public void run(String... args) throws Exception {

        studentRepository.deleteAllInBatch();
        subjectRepository.deleteAllInBatch();


        Student student1 = new Student("John","Doe");//create new staff
        Student student2 = new Student("Mike","Smith");
        Student student3 = new Student("Bob","Test");
        //studentRepository.save(student); //use dao service to save to the database
        //log.info("New customer created " + student);


        Subject subject1 = new Subject("Math", 12);
        Subject subject2 = new Subject("P.E", 12);
        Subject subject3 = new Subject("English", 20);
        //Subject subj2 = new Subject("Math", 12);

        subject1.getStudents().add(student1);
        student1.getSubjects().add(subject1);

        subject1.getStudents().add(student2);
        student2.getSubjects().add(subject1);
        student2.getSubjects().add(subject2);
        student2.getSubjects().add(subject3);

        //subj1.getStudents().add(student);
        //subj2.getStudents().add(student);

        subjectRepository.save(subject1);

        List<Student> allStudents = studentRepository.findAll();
        log.info("All Customers " + allStudents);
    }
}

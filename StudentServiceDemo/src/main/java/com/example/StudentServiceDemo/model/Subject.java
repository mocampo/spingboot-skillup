package com.example.StudentServiceDemo.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Getter @Setter
@NoArgsConstructor
@Entity
@Table(name = "subjects")
public class Subject {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String subjName;

    private int credits;

    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE}, mappedBy = "subjects")
    //@JsonIgnoreProperties({"subjects", "id", "- students"})
    @JsonIgnore
    private Set<Student> students = new HashSet<>();

    public Subject(String subjName, int credits) {
        this.subjName = subjName;
        this.credits = credits;
    }

}

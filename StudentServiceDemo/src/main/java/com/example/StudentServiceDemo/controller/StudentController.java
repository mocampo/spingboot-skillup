package com.example.StudentServiceDemo.controller;

import com.example.StudentServiceDemo.ResourceNotFoundException;
import com.example.StudentServiceDemo.model.Student;
import com.example.StudentServiceDemo.model.Subject;
import com.example.StudentServiceDemo.repository.StudentRepository;
import com.example.StudentServiceDemo.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
public class StudentController {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    SubjectRepository subjectRepository;

    //Get request for all student info
    @GetMapping("/students")
    List<Student> findAll(){
        return studentRepository.findAll();
    }

    //Get request by student id
    @GetMapping("/students/{id}")
    Optional<Student> findOne(@PathVariable Long id){
        return studentRepository.findById(id);
    }

    //Post request to add new student
    @PostMapping("/students")
    Student newStudent(@RequestBody Student newStudent){
        return studentRepository.save(newStudent);
    }

    //Put Request to update existing record
    @PutMapping("students/{id}")
    Student update(@RequestBody Student newStudent, @PathVariable Long id){
        return studentRepository.findById(id).map(x->{
            x.setFirstName(newStudent.getFirstName());
            x.setLastName(newStudent.getLastName());
            return studentRepository.save(x);
        }).orElseGet(()->{
            newStudent.setId(id);
            return studentRepository.save(newStudent);
        });
    }

    //Delete student record
    @DeleteMapping("/student/{id}")
    void deleteStudent(@PathVariable Long id){
        studentRepository.deleteById(id);
    }

    //Get the student and display their subjects
    @GetMapping("/students/{studentId}/subjects")
    Set<Subject> getSubjects(@PathVariable Long studentId){
        return studentRepository.findById(studentId).get().getSubjects();
    }

    //Add subjects to student
    @PostMapping("/students/{studentId}/subjects/{subjectId}")
    Set<Subject> addSubject(@PathVariable Long studentId, @PathVariable Long subjectId){
        Subject subject = subjectRepository.findById(subjectId).orElseThrow(()-> new ResourceNotFoundException("Subject", subjectId));

        return studentRepository.findById(studentId).map(student -> {
            student.getSubjects().add(subject);
        return studentRepository.save(student).getSubjects();
        }).orElseThrow(() -> new ResourceNotFoundException("Student",studentId));
    }

}

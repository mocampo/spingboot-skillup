package com.example.StudentServiceDemo.controller;

import com.example.StudentServiceDemo.model.Subject;
import com.example.StudentServiceDemo.repository.StudentRepository;
import com.example.StudentServiceDemo.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class SubjectController {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    SubjectRepository subjectRepository;

    @GetMapping("/subjects")
    List<Subject> getAll(){
        return subjectRepository.findAll();
    }

    @GetMapping("/subjects/{id}")
    Optional<Subject> getOne(@PathVariable Long id){
        return subjectRepository.findById(id);
    }

    @PostMapping("/subjects")
    Subject newSubject(@RequestBody Subject newSubject){
        return subjectRepository.save(newSubject);
    }

    @PutMapping("subjects/{id}")
    Subject update(@RequestBody Subject newSubject, @PathVariable Long id){
        return subjectRepository.findById(id).map(x->{
            x.setSubjName(newSubject.getSubjName());
            x.setCredits(newSubject.getCredits());
            return subjectRepository.save(x);
        }).orElseGet(()->{
            newSubject.setId(id);
            return subjectRepository.save(newSubject);
        });
    }

    @DeleteMapping("/subjects/{id}")
    void deleteSubject(@PathVariable Long id){
        subjectRepository.deleteById(id);
    }
}

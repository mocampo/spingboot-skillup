package com.example.StudentServiceDemo;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(){}

    public ResourceNotFoundException(String entity, Long id){
        super(entity + "id" + id + "not found");
    }
}

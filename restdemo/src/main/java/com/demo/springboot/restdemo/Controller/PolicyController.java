package com.demo.springboot.restdemo.Controller;

import com.demo.springboot.restdemo.Entity.Policy;
import com.demo.springboot.restdemo.Service.PolicyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class PolicyController {

    @Autowired
    PolicyRepository repository;

    //GET all policies
    @GetMapping("/policies")
    List<Policy> findAll(){
        return repository.findAll();
    }

    //POSt new policy
    @PostMapping("/policies")
    Policy newPolicy(@RequestBody Policy newPolicy){
        return repository.save(newPolicy);
    }

    //Get by id no
    @GetMapping("/policies/{id}")
    Optional<Policy> findOne(@PathVariable Long id){
        return repository.findById(id);
    }

    //Update entry
    @PutMapping("/policies/{id}")
    Policy update(@RequestBody Policy newPolicy, @PathVariable Long id){
        return repository.findById(id).map(x -> {
            x.setName(newPolicy.getName());
            x.setConditions(newPolicy.getConditions());
            return repository.save(x);
        }).orElseGet(()-> {
            newPolicy.setId(id);
            return repository.save(newPolicy);
        });
    }

    //Delete entry
    @DeleteMapping("policies/{id}")
    void deletePolicy(@PathVariable Long id){
        repository.deleteById(id);
    }
}

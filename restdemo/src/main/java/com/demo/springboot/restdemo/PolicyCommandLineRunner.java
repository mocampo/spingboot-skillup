package com.demo.springboot.restdemo;

import com.demo.springboot.restdemo.Entity.Policy;
import com.demo.springboot.restdemo.Service.PolicyRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class PolicyCommandLineRunner implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(PolicyCommandLineRunner.class);

    @Autowired
    private PolicyRepository policyRepository;

    //insert initial values
    @Override
    public void run(String... args) throws Exception {
        Policy policy = new Policy("John", "cond1"); //create new staff
        policyRepository.save(policy); //use dao service to save to the database
        log.info("New staff member created " + policy);

        List<Policy> allStaff = policyRepository.findAll();
        log.info("All Staff members " + allStaff);
    }
}

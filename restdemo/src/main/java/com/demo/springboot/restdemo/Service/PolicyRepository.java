package com.demo.springboot.restdemo.Service;

import com.demo.springboot.restdemo.Entity.Policy;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PolicyRepository extends JpaRepository<Policy, Long> {
}

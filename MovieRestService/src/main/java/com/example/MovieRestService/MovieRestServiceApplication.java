package com.example.MovieRestService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovieRestServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieRestServiceApplication.class, args);
	}

}

package com.example.MovieRestService.service;

import com.example.MovieRestService.MovieUrl;
import com.example.MovieRestService.configuration.RestTemplateConfig;
import com.example.MovieRestService.model.Data;
import com.example.MovieRestService.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MovieService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    RestTemplateConfig restTemplateConfig;

    public List<String> getMovieTitle(String movieName, int pageNo){
        List<Data> movieList = new ArrayList<>();

        //get full url string ie movie title + page number
        String movieSearchUrl = fullSearchUrl(movieName, pageNo);

        //getForObject used to invoke the service at the given url/endpoint
        Movie movieResponse = restTemplate.getForObject(movieSearchUrl, Movie.class);

        //Add response to the movie list array
        movieList.addAll(movieResponse.getData());

        //map to string and filter titles
        List<String> title = movieList.stream().map(Data::getTitle).collect(Collectors.toList());

        //Sort the array in ascending order
        Collections.sort(title);

        return title;
    }

    public String fullSearchUrl(String movieName, int pageNo){
        return restTemplateConfig.getBaseUrl() + "?Title=" +movieName + "&page=" + pageNo;
    }

}

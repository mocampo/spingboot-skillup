package com.example.MovieRestService.service;

import com.example.MovieRestService.configuration.RestTemplateConfig;
import com.example.MovieRestService.model.Data;
import com.example.MovieRestService.model.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

//Class to handle all titles and sort
@Service
public class TitleService {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    RestTemplateConfig restTemplateConfig;

    /*
    public List<String> getTitleList(){
        List<Data> titleList = new ArrayList<>();

        //get full url string ie movie title + page number
        String titleSearchUrl = restTemplateConfig.getBaseUrl();

        //getForObject used to invoke the service at the given url/endpoint
        Movie titleResponse = restTemplate.getForObject(titleSearchUrl, Movie.class);

        //Add response to the movie list array
        titleList.addAll(titleResponse.getData());

        //map to string and filter titles
        List<String> title = titleList.stream().map(Data::getTitle).filter(t -> t.contains("Waterworld")).collect(Collectors.toList());

        //Sort the array in ascending order
        Collections.sort(title);

        return title;
    }

     */


    public List<String> getTitleList(String titleStr) {
        List<Data> titleList = new ArrayList<>();

        int pageNo = 1;

        List<String> title = new ArrayList<>();

        do {
            //get full url string ie movie title + page number
            String titleSearchUrl = restTemplateConfig.getBaseUrl() + "?page=" + pageNo;

            //getForObject used to invoke the service at the given url/endpoint
            Movie titleResponse = restTemplate.getForObject(titleSearchUrl, Movie.class);

            //Add response to the movie list array
            titleList.addAll(titleResponse.getData());

            //map to string and filter titles
            title = titleList.stream().map(Data::getTitle).filter(t -> t.contains(titleStr)).collect(Collectors.toList());

            //Sort the array in ascending order
            //Collections.sort(title);
            pageNo++;

        } while (pageNo < 278);

        return title;


    }

}

package com.example.MovieRestService.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie {

    @Getter @Setter
    @JsonProperty(value= "page")
    private int page;

    @Getter @Setter
    @JsonProperty(value = "per_page")
    private String per_page;

    @Getter @Setter
    @JsonProperty(value = "total")
    private int total;

    @Getter @Setter
    private List<Data> data;

    @Override
    public String toString() {
        return "Movie{" +
                "page='" + page + '\'' +
                ", per_page='" + per_page + '\'' +
                ", total=" + total +
                ", data=" + data +
                '}';
    }
}

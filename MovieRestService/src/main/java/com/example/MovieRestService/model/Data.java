package com.example.MovieRestService.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    @Getter @Setter
    @JsonProperty(value = "Title")
    private String title;

    @Getter @Setter
    @JsonProperty(value = "Year")
    private String year;

    @Getter @Setter
    private String imdbID;

    @Override
    public String toString() {
        return "Data{" +
                "title='" + title + '\'' +
                ", year='" + year + '\'' +
                ", imdbID='" + imdbID + '\'' +
                '}';
    }
}

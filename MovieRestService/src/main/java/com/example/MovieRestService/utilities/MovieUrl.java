package com.example.MovieRestService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MovieUrl {

    @Value("${movie.url}")
    private String searchUrl;

    public String fullSearchUrl(String movieName, int pageNo){
        return searchUrl + movieName + "&page=" + pageNo;
    }
}

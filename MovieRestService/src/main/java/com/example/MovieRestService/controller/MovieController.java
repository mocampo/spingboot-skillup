package com.example.MovieRestService.controller;

import com.example.MovieRestService.service.MovieService;
import com.example.MovieRestService.service.TitleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MovieController {

    @Autowired
    MovieService movieService;

    @Autowired
    TitleService titleService;

    @RequestMapping(value = "/{subStr}/{page}")
    public List<String> getMovieOperation(@PathVariable String subStr, @PathVariable int page) {

        return movieService.getMovieTitle(subStr, page);
    }

    @RequestMapping(value = "/titles/{titleStr}")
    public List<String> getMovieOperation(@PathVariable String titleStr) {

        return titleService.getTitleList(titleStr);
    }
}

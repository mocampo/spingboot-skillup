package com.skillrampup.demo;

import com.skillrampup.demo.jpa.Staff;
import com.skillrampup.demo.service.StaffRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

//used to save staff to the database using jpa respository
@Component
public class StaffRepositoryCommandLineRunner implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(StaffRepositoryCommandLineRunner.class);

    @Autowired
    private StaffRepository staffRepository;

    @Override
    public void run(String... args) throws Exception {
        Staff staff = new Staff("John", "dept1"); //create new staff
        staffRepository.save(staff); //use dao service to save to the database
        log .info("New staff member created " + staff);

        List<Staff> allStaff = staffRepository.findAll();
        log .info("All Staff members " + allStaff);


    }
}

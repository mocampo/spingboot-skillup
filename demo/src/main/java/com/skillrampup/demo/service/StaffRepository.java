package com.skillrampup.demo.service;

import com.skillrampup.demo.jpa.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

//JPA repsoitory
public interface StaffRepository extends JpaRepository<Staff, Long> {
}

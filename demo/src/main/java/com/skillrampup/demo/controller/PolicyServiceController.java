package com.skillrampup.demo.controller;


import com.skillrampup.demo.model.Policy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class PolicyServiceController {
    private static Map<String, Policy> policyRepo = new HashMap<>();
    static{
        Policy health = new Policy();
        health.setId("1");
        health.setName("Health");
        policyRepo.put(health.getId(), health);

        Policy life = new Policy();
        life.setId("2");
        life.setName("Life");
        policyRepo.put(life.getId(), life);
    }

    @RequestMapping(value = "/policies/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> delete(@PathVariable("id") String id){
        policyRepo.remove(id);
        return new ResponseEntity<>("Policy successfully deleted", HttpStatus.OK);
    }

    @RequestMapping(value = "/policies/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Object> updatePolicy(@PathVariable("id") String id, @RequestBody Policy policy){
        policyRepo.remove(id);
        policy.setId(id);
        policyRepo.put(id, policy);
        return new ResponseEntity<>("Policy successfully updated", HttpStatus.OK);
    }

    //POST
    @RequestMapping(value = "/policies", method = RequestMethod.POST)
    public ResponseEntity<Object> createPolicy(@RequestBody Policy policy){
        policyRepo.put(policy.getId(), policy);
        return new ResponseEntity<>("Policy successfully createf", HttpStatus.CREATED);
    }

    //Get
    @RequestMapping(value = "/policies")
    public ResponseEntity<Object> getPolicy(){
        return new ResponseEntity<>(policyRepo.values(), HttpStatus.OK);
    }

}
